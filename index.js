/*
*
* LIBRERIAS
*
* */


const mysql = require('mysql');
const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const {decode, encode} = require('jwt-simple');
const jwt = require('jsonwebtoken');
const secret = 'formulasecreta';
app.use(bodyparser.json());




/*
*
* CONEXION CON LA BASE DE DATOS
*
* */


/*ACCESO A LA DB*/
let mysqlConnection = mysql.createConnection({
    host: '',
    port: '',
    user: '',
    password: '',
    database: '',
    multipleStatements: true
});
mysqlConnection.connect((error) => {
    if (!error) {
        console.log("Conexion exitosa con la base de datos.");
    } else {
        console.log("La conexion con la base de datos fallo.");
        console.log("Error: " + JSON.stringify(error, undefined, 2));
    }
});


/*APLICACION INICIADA*/
app.listen(3000, () => console.log("El servidor fue iniciado en el puerto 3000."));




/*
*
* ¡¡¡MUY IMPORTANTE!!!
*
* VARIABLES GLOBALES CON DATOS RETORNADOS
* ATENCION: estas variables deberian retornarse al iniciar sesion y guardarse en el session/local storagge del navegador;
* caso contrario, siempre quedaria guardado en el servidor. Estos datos deberian pasarse de forma CIFRADA para que
* no puedan "jugar" extraños con la APP. Por ejemplo 'esta_logeado' y 'es_admin' podrian ser dos palabras secretas encriptadas
* y el 'usuario_id' podria ser la primary key autoincremental encriptada o sino el dni como primary key unique index.
* En este caso, utilizo estas variables globales porque el proyecto pide hacer solo el backend. En el caso que fuese
* un proyecto real, esto NO deberia estar aca por el motivo expuesto.
* Por favor considerar al simple efecto de hacer funcional la aplicacion y no que es un error de programacion!!
*
* */


let esta_logeado = 0; // 0 es no y 1 es si
let usuario_id; // id en la base (primary key autoincremental)
let es_admin = 0; // 0 es no, 1 es si y 2 es el super admin
let nombre;
let apellido;
let direccion;
let telefono;




/*
*
* END POINTS DE USUARIOS
*
* */


/*INGRESO DE USUARIOS*/
app.get('/', (request, response) => {
    if(esta_logeado === 1) {
        response.send("La sesion esta iniciada. Si necesitas ingresar con otra cuenta, deberas salir de la sesion.");
    } else {
        let ingreso = request.body;
        let token;
        let clave_decodificada;
        mysqlConnection.query('SELECT * FROM usuario WHERE usuario = ?', [ingreso.usuario], (error, rows) => {
            if (!error) {
                token = rows[0].clave; // obtiene la clave encriptada
                clave_decodificada = jwt.decode(token, secret); // la decodifica y en el IF la compara
                if(ingreso.usuario === rows[0].usuario && ingreso.clave === clave_decodificada) {
                    esta_logeado = 1; // 1 significa estar logueado y 0 no estarlo
                    usuario_id = rows[0].id;
                    es_admin = rows[0].rol; // 1 significa ser admin y 0 no serlo (el admin se crea desde el script de la DB)
                    nombre = rows[0].nombre;
                    apellido = rows[0].apellido;
                    direccion = rows[0].direccion;
                    telefono = rows[0].telefono;
                    response.send(rows);
                } else {
                    response.send("El usuario y/o clave ingresados sin incorrectos");
                }
            } else {
                response.send(error);
            }
        })
    }
});


/*CERRAR SESION*/
app.post('/salir', (request, response) => {
    if(esta_logeado === 1) {
        esta_logeado = 0;
        usuario_id = 0;
        es_admin = 0;
        nombre = null;
        apellido = null;
        direccion = null;
        telefono = null;
        response.send("Logout exitoso.");
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*REGISTRO DE USUARIOS*/
app.post('/registro', (request, response) => {
    const id = 0; // si paso 0 da de alta, sino modifica (por procedimiento almacenado)
    const rol = 0; // rol 0 es cliente y 1 es administrador
    let registro = request.body;
    const sql = "SET @id = ?; SET @rol = ?; SET @nombre = ?; SET @apellido = ?; SET @telefono = ?; SET @email = ?; SET @direccion = ?; SET @usuario = ?; SET @clave = ?; \
    CALL AgregarEditarUsuario(@id, @rol, @nombre, @apellido, @telefono, @email, @direccion, @usuario, @clave);";
    mysqlConnection.query(sql, [id, rol, registro.nombre, registro.apellido, registro.telefono, registro.email, registro.direccion, registro.usuario, encode(registro.clave, secret)], (error, rows) => {
        if (!error) {
            rows.forEach(element => {
                if(element.constructor === Array)
                    response.send("Cliente ID " + element[0].id + " agregado con exito.");
            });
        } else {
            response.send(error); // un error posible es que el usuario ya se encuentre utilizado
        }
    })
});




/*
*
* END POINT MENU
*
* */


/*LISTAR TODOS LOS PLATOS*/
app.get('/menu', (request, response) => {
    if(esta_logeado === 1) {
        mysqlConnection.query('SELECT * FROM menu', (error, rows) => {
            if (!error) {
                response.send(rows); // cualquier usuario puede ver todos los platos, siempre que este logueado
            } else {
                response.send(error);
            }
        })
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*LISTAR PLATOS POR ID*/
app.get('/menu/:id', (request, response) => {
    if(esta_logeado === 1) {
        mysqlConnection.query('SELECT * FROM menu WHERE id = ?', [request.params.id], (error, rows) => {
            if (!error) {
                response.send(rows); // cualquier usuario puede ver un plato por ID, siempre que este logueado
            } else {
                response.send(error);
            }
        })
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*ALTA DE PLATOS*/
app.post('/menu', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 1 || es_admin === 2) {
            const id = 0;
            let menu = request.body;
            const sql = "SET @id = ?; SET @plato = ?; SET @precio = ?; \
    CALL AgregarEditarMenu(@id, @plato, @precio);";
            mysqlConnection.query(sql, [id, menu.plato, menu.precio], (error, rows) => {
                if (!error) {
                    rows.forEach(element => {
                        if(element.constructor === Array) {
                            response.send("Menu ID " + element[0].id + " agregado con exito.");
                        }
                    });
                } else {
                    response.send(error);
                }
            })
        } else {
            response.send("Acceso solo para administradores.");
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*MODIFICAR PLATO*/
app.put('/menu', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 1 || es_admin === 2) {
            let menu = request.body;
            const sql = "SET @id = ?; SET @plato = ?; SET @precio = ?; \
    CALL AgregarEditarMenu(@id, @plato, @precio);";
            mysqlConnection.query(sql, [menu.id, menu.plato, menu.precio], (error) => {
                if (!error) {
                    response.send("Plato modificado con exito.");
                } else {
                    response.send(error);
                }
            })
        } else {
            response.send("Acceso solo para administradores.");
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*BORRAR PLATO*/
app.delete('/menu/:id', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 1 || es_admin === 2) {
            mysqlConnection.query('DELETE FROM menu WHERE id = ?', [request.params.id], (error) => {
                if (!error) {
                    response.send("Plato eliminado con exito.");
                } else {
                    response.send(error);
                }
            })
        } else {
            response.send("Acceso solo para administradores.");
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});




/*
*
* END POINT PEDIDOS
*
* */


/*LISTAR PEDIDOS PARA EL USUARIO*/
app.get('/mispedidos', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 1 || es_admin === 2) {
            response.send("Seccion solo para clientes.");
        } else {
            mysqlConnection.query('SELECT * FROM pedido WHERE usuario = ?', [usuario_id], (error, rows) => {
                if (!error) {
                    response.send(rows);
                } else {
                    response.send(error);
                }
            })
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*LISTAR PEDIDOS*/
app.get('/pedidos', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 1 || es_admin === 2) {
            mysqlConnection.query('SELECT * FROM pedido WHERE estado<5', (error, rows) => {
                if (!error) {
                    response.send(rows); // no se muestran pedidos entregados ni cancelados
                } else {
                    response.send(error);
                }
            })
        } else {
            response.send("Acceso solo para administradores.");
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*LISTAR PEDIDOS POR ID*/
app.get('/pedidos/:id', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 1 || es_admin === 2) {
            mysqlConnection.query('SELECT * FROM pedido WHERE id = ? AND estado<5', [request.params.id], (error, rows) => {
                if (!error) {
                    response.send(rows); // no se muestran pedidos entregados ni cancelados
                } else {
                    response.send(error);
                }
            })
        } else {
            response.send("Acceso solo para administradores.");
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*REALIZAR UN PEDIDO*/
// Para realizar un pedido, desde el front end se deberia obtener el menu y realizar funciones en JS que vaya "agregando" los productos al "carrito".
// Una vez que se tenga listo, se deberia hacer un post con el total ya calculado y los productos pasados a @productos
app.post('/pedidos', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 1 || es_admin === 2) {
            response.send("Los administradores no pueden realizar pedidos. El pedido debe ser realizado desde una cuenta personal.");
        } else {
            let ide = 0;
            let estado = 0;
            let menu = request.body;
            const sql = "SET @id = ?; SET @usuario = ?; SET @nombre = ?; SET @apellido = ?; SET @direccion = ?; SET @telefono = ?; SET @productos = ?; SET @total = ?; SET @medio_pago = ?; SET @estado = ?; \
    CALL AgregarEditarPedido(@id, @usuario, @nombre, @apellido, @direccion, @telefono, @productos, @total, @medio_pago, @estado);";
            // Tanto el medio de pago como el estado son enteros que se deben interpretar por el frontend segun:
            // Medio de pago: 1) efectivo 2) debito 3) credito 4) mercadopago
            // Estado del pedido: 1) nuevo 2) confirmado 3) preparando 4) enviado 5) entregado 6) cancelado
            mysqlConnection.query(sql, [ide, usuario_id, nombre, apellido, direccion, telefono, menu.productos, menu.importe, menu.mediopago, estado], (error) => {
                if (!error) {
                    response.send("El pedido fue cargado con exito.");
                } else {
                    response.send(error);
                }
            })
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*CAMBIAR ESTADO DE UN PEDIDO*/
app.put('/pedidos', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 1 || es_admin === 2) {
            let modificar = request.body;
            const sql = "SET @id = ?; SET @usuario = ?; SET @nombre = ?; SET @apellido = ?; SET @direccion = ?; SET @telefono = ?; SET @productos = ?; SET @total = ?; SET @medio_pago = ?; SET @estado = ?; \
    CALL AgregarEditarPedido(@id, @usuario, @nombre, @apellido, @direccion, @telefono, @productos, @total, @medio_pago, @estado);";
            mysqlConnection.query(sql, [modificar.id, modificar.usuario, modificar.nombre, modificar.apellido, modificar.direccion, modificar.telefono, modificar.productos, modificar.total, modificar.medio_pago, modificar.estado], (error) => {
                if (!error) {
                    response.send("Estado del producto cambiado con exito."); // el admin solo puede cambiar el estado del pedido, no puede cambiar otros datos
                } else {
                    response.send(error);
                }
            })
        } else {
            response.send("Acceso solo para administradores.");
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*BORRAR PEDIDO*/
app.delete('/pedidos/:id', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 1 || es_admin === 2) {
            mysqlConnection.query('DELETE FROM pedido WHERE id = ?', [request.params.id], (error) => {
                if (!error) {
                    response.send("Pedido eliminado con exito.");
                } else {
                    response.send(error);
                }
            })
        } else {
            response.send("Acceso solo para administradores.");
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});




/*
*
*
* END POINTS DUEÑO DEL RESTAURANTE (super admin)
* SOLO PUEDE VER LA PARTE "GERENCIAL" QUE SERIA CUANTO VENDE Y CUANTOS LE CANCELAN
* NO SE PIDE EN LA CONSIGNA PERO DADO QUE ESTOS ESTADOS NO DEBERIAN SER MOSTRADO EN COCINA, LOS MUESTRO AQUI
*
* */


/*LISTAR PEDIDOS CANCELADOS*/
app.get('/admin/ventas', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 2) {
            mysqlConnection.query('SELECT * FROM pedido WHERE estado = 5', (error, rows) => {
                if (!error) {
                    response.send(rows);
                } else {
                    response.send(error);
                }
            })
        } else {
            response.send("Acceso solo para super administradores.");
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});


/*LISTAR PEDIDOS CANCELADOS*/
app.get('/admin/pedidoscancelados', (request, response) => {
    if(esta_logeado === 1) {
        if(es_admin === 2) {
            mysqlConnection.query('SELECT * FROM pedido WHERE estado = 6', (error, rows) => {
                if (!error) {
                    response.send(rows);
                } else {
                    response.send(error);
                }
            })
        } else {
            response.send("Acceso solo para super administradores.");
        }
    } else {
        response.send("No se registra ingreso al sistema.");
    }
});