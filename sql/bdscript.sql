# Creacion de la tabla USUARIO
create table usuario
(
	id int auto_increment,
	rol int,
	nombre varchar(30),
	apellido varchar(30),
	telefono varchar(10),
	email varchar(50),
	direccion varchar(100),
	usuario varchar(50),
	clave varchar(200),
	constraint usuario_pk
		primary key (id)
);
create unique index usuario_usuario_uindex
	on usuario (usuario);


# Crea un usuario super administrador (dueño del restaurante)
# El dueño puede ver todo lo del administrador y tambien pedidos cancelados y ventas
# Usuario: superman
# Clave: acamica (encriptada)
insert into usuario (rol, usuario, clave) values (2,'superman', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.ImFjYW1pY2Ei.waDqzpPn9IRYpDcwq6RI0GTm8ZcvRCNr3uviCTJfhGU');


# Crea un usuario administrador
# Usuario: delilah
# Clave: acamica (encriptada)
insert into usuario (rol, usuario, clave) values (1,'delilah', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.ImFjYW1pY2Ei.waDqzpPn9IRYpDcwq6RI0GTm8ZcvRCNr3uviCTJfhGU');


# Creacion de la tabla MENU
create table menu
(
	id int auto_increment,
	plato varchar(50),
	precio float,
	constraint menu_pk
		primary key (id)
);


# Creacion de la tabla PEDIDO
create table pedido
(
	id int auto_increment,
	usuario int,
	nombre varchar(30),
	apellido varchar(30),
	direccion varchar(50),
	telefono varchar(10),
	productos varchar(200),
	total float,
	medio_pago int,
	estado int,
	constraint pedido_pk
		primary key (id)
);


# Procedimiento almacenado: Agregar-Editar USUARIO
create procedure AgregarEditarUsuario (
in _id int,
in _rol int,
in _nombre varchar(30),
in _apellido varchar(30),
in _telefono varchar(10),
in _email varchar(50),
in _direccion varchar(50),
in _usuario varchar(50),
in _clave varchar(50)
)
begin
    if _id = 0 then
        insert into usuario (rol, nombre, apellido, telefono, email, direccion, usuario, clave)
        values (_rol, _nombre, _apellido, _telefono, _email, _direccion, _usuario, _clave);
        set _id = last_insert_id();
    else
        update usuario
            set
                telefono = _telefono,
                email = _email,
                direccion = _direccion,
                clave = _clave
        where id = _id;
    end if;
    select _id as 'id';
end;


# Procedimiento almacenado: Agregar-Editar MENU
create procedure AgregarEditarMenu (
in _id int,
in _plato varchar(50),
in _precio float
)
begin
    if _id = 0 then
        insert into menu (plato, precio)
        values (_plato, _precio);
        set _id = last_insert_id();
    else
        update menu
            set
                plato = _plato,
                precio = _precio
        where id = _id;
    end if;
    select _id as 'id';
end;


# Procedimiento almacenado: Agregar-Editar PEDIDO
create procedure AgregarEditarPedido (
in _id int,
in _usuario int,
in _nombre varchar(30),
in _apellido varchar(30),
in _direccion varchar(50),
in _telefono varchar(10),
in _productos varchar(200),
in _total float,
in _medio_pago int,
in _estado int
)
begin
    if _id = 0 then
        insert into pedido (usuario, nombre, apellido, direccion, telefono, productos, total, medio_pago, estado)
        values (_usuario, _nombre, _apellido, _direccion, _telefono, _productos, _total, _medio_pago, _estado);
        set _id = last_insert_id();
    else
        update pedido
            set
                estado = _estado
        where id = _id;
    end if;
    select _id as 'id';
end;