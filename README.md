Para poder ejecutar esta aplicacion, los pasos son muy sencillos.

Primero debes descargar el repositorio. 
Una vez descargado, abrir el script de la base de datos y ejecutarlo. En mi caso, utilizo JetBrains DataGrip.
Desde el script se crean dos usuarios, debidamente comentado.

Segundo, debes abrir el proyecto en tu IDE favorito - en mi caso, JetBrains WebStorm - y en el terminal del IDE (o la de tu propia distro) ejecutar los siguientes comandos:

* npm init
* npm i npm
* npm i express
* npm i mysql
* npm i body-parser
* npm install jsonwebtoken
* npm install jwt-simple
* nodemon index.js

Los comandos anteriormente mencionados, instalaran las librerias necesarias para ejecutar la APP.

Por ultimo, debes cambiar las credenciales por las de tu servidor local de MySQL o uno remoto (como puede ser www.freemysqlhosting.net).
Y, luego, ejecutar un último comando:

* node index.js

Listo... Ahora podes ver funcionar la APP en localhost:3000 o el puerto que hayas decidido utilizar.

Para realizar las pruebas que creas necesarias, podes utilizar Insomnia (www.insomnia.rest).
Un entorno de produccion simple y, en principio, gratuito, podria ser Heroku (www.heroku.com).
